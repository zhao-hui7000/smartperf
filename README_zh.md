# SmartPerf

## 简介

- **SmartPerf** 是基于OpenHarmony系统开发的性能、功耗测试工具链，支持两个组成部分：设备端的Device工具和PC端的Host工具。
- **Device工具：** 是一款初级的、粗粒度的数据采集性能、功耗测试工具，支持帧率、功耗、热、Soc信息的实时采集、实时展示、数据报告生成。
- **Host工具：** 是一款深入挖掘数据、细粒度的展示数据的性能、功耗测试工具，支持CPU调度、频点、进程线程时间片、堆内存、FPS数据采集和展示，支持在泳道图中展示非实时的采集数据，支持GUI操作数据分析。

其主要的结构如下图所示：

![系统架构图](figures/SmartPerf.png)


## 目录
```
/smartperf
├── device                        # device端 相关代码
│   ├── device_command            # device端 命令行方式采集代码
│   ├── device_ui                 # device端 GUI方式采集代码
│   ├── figures                   # device端 相关截图
├── figures                       # 相关截图
├── host                          # host端 相关代码
│   ├── doc                       # host端 相关使用文档
│   ├── ide                       # host端 IDE 模块目录
│   │    └── src                  # 主机测调优模块代码
│   │    │   ├── base-ui          # 基础组件目录
│   │    │   └── Trace            # 业务逻辑目录
│   ├── trace_streamer            # 解析模块代码目录
│   │    ├── base                 # 基础功能
│   │    ├── cfg                  # 配置目录
│   │    ├── filter               # Filter功能
│   │    ├── include              # Include头文件
│   │    ├── multi_platform       # 平台适配
│   │    ├── parser               # 解析业务逻辑
│   │    │   ├── bytrace_parser   # byTrace解析业务逻辑
│   │    │   └── htrace_parser    # hTrace 解析业务逻辑
│   │    ├── table                # 表结构
│   │    ├── trace_data           # trace 结构
│   │    ├── trace_streamer       # traceStreamer 结构
│   │    │   └── kits             # js/napi接口存放目录
```



## 约束
host端构建约束
- 语言版本
    - C++11或以上
    - TypeScript 4.2.3


## 相关文档

- [SmartPerf Device工具使用说明](device/README_zh.md)
- [SmartPerf Device工具客户端使用说明](device/device_ui/README_zh.md)
- [SmartPerf Device工具服务端使用说明](device/device_command/README_zh.md)
- [SmartPerf Host工具的运行和使用方法](host/doc/quickstart_smartperf.md) 
- [SmartPerf Host工具编译网页可运行环境](host/doc/compile_smartperf.md) 
- [SmartPerf Host工具编译数据解析文件](host/doc/compile_trace_streamer.md)